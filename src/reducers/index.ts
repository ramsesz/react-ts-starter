import { firebaseStateReducer } from "react-redux-firebase";
import { routerReducer } from "react-router-redux";
import { combineReducers } from "redux";

// import busy from "./busy";
// import device from "./device";
// import geo from "./geo";
// import incident from "./incident";
// import processes from "./processes";
// import resource from "./resource";
// import ui from "./ui";

// Build the middleware for intercepting and dispatching navigation actions

const rootReducer = combineReducers({
  firebase: firebaseStateReducer,
  router: routerReducer
});

export default rootReducer;
