// import { IConfig } from "../../domain";
import history from "../../store/history";

// declare var Config: IConfig;

export function historyPush(url: string) {
  console.log("push: " + url);
  if (url) {
    history.push(url);
  }
}
