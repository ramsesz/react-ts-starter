import { Action, Dispatch } from "redux";

export interface IDispatcheableProperties {
  dispatch?: Dispatch<Action>;
}

export interface IWaitableProperties {
  busy?: boolean;
}

export interface IRoutableProperties {
  match?: {
    isExact: boolean;
    params: { id: string; step: string };
    path: string;
    url: string;
  };
  history?: {
    goBack: () => void;
    push: (path: string) => void;
  };
  location?: {
    hash: string;
    pathname: string;
    search: string;
    state?: any;
  };
}

export interface IFormProperties {
  asyncValidate?: () => void;
  asyncValidating?: string | boolean;
  destroy?: () => void;
  dirty?: boolean;
  error?: string;
  formValueSelector?: (name: string) => any;
  handleSubmit?: (data: any) => void;
  initialValues?: any;
  initialize?: (data: any) => void;
  invalid?: boolean;
  pristine?: boolean;
  reset?: () => void;
  submitFailed?: boolean;
  submitting?: boolean;
  touch?: (fields: string[]) => void;
  untouch?: (fields: string[]) => void;
  valid?: boolean;
}
