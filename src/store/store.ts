import * as firebase from "firebase";
import { History } from "history";
import { reactReduxFirebase } from "react-redux-firebase";
import { routerMiddleware } from "react-router-redux";
import { applyMiddleware, compose, createStore, Reducer, Store } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import createSagaMiddleware from "redux-saga";

import { fireBaseConfig } from "../config/config";
import rootReducer from "../reducers";
import rootSaga from "../sagas";
import { IState } from "../typings/state";

firebase.initializeApp(fireBaseConfig); // initialize firebase instance

const reduxFirebaseConfig = { userProfile: "users" };

export default function configureStore(
  initialState: IState = {},
  history: History
) {
  // Add redux Firebase to compose
  const createStoreWithFirebase: (
    reducer: Reducer<{}>,
    compose: any
  ) => Store<any> = compose(reactReduxFirebase(firebase, reduxFirebaseConfig))(
    createStore
  );

  const sagaMiddleware = createSagaMiddleware();

  const store: Store<any> = createStoreWithFirebase(
    rootReducer,
    composeWithDevTools(
      applyMiddleware(routerMiddleware(history)),
      applyMiddleware(sagaMiddleware)
    )
  );

  // Hook up the saga middleware
  sagaMiddleware.run(rootSaga);

  return store;
}
