import { withRouter } from "react-router";

/**
 * Decorator function to wrap a component with withRouter, which is in fact a decorator.
 * Remark: use it on the top-level element just below the <Router>...
 */
export default function routed(): any {
  return (target: any) => withRouter(target);
}
