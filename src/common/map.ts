import { connect } from "react-redux";
import { Action, Dispatch } from "redux";

import { IState } from "../typings/state";

type mapStateFunction<P> = (state: IState, props: P) => P;
type mapDispatchFunction<P> = (
  dispatch: Dispatch<Action>,
  props: P
) => P | undefined;

/**
 * The connect function has incorrect typings to use it as a decorator, although it IS
 * a decorator. This solution is inspired by:
 * https://github.com/Microsoft/TypeScript/issues/9365
 *
 * @param mapStateToProps
 * @param mapDispatchToProps
 */
export default function map<P>(
  mapStateToProps?: mapStateFunction<P>,
  mapDispatchToProps?: mapDispatchFunction<P>
): any {
  return (target: any) =>
    connect(mapStateToProps, mapDispatchToProps)(target) as any;
}
