import { createBrowserHistory, History } from "history";

/**
 * React-router-redux is not yet ready
 */
const history: History = createBrowserHistory();
export default history;
