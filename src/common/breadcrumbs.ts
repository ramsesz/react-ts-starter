import { IAppRoute, IBreadCrumb } from "../typings/inav";

export function getBreadCrumbs(routes: IAppRoute[]) {
  const crumbs: IBreadCrumb = routes.reduce((result, item) => {
    result[item.path] = item.title;
    return result;
    // tslint:disable-next-line:align
  }, {});
  return crumbs;
}
