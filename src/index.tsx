import "./index.css";

import { createBrowserHistory, History } from "history";
import * as React from "react";
import * as ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { ConnectedRouter } from "react-router-redux";
import { Store } from "redux";

import App from "./components/app/App";
import AppRoutes from "./components/app/appRoutes";
import registerServiceWorker from "./registerServiceWorker";
import configureStore from "./store/store";

const history: History = createBrowserHistory();
const store: Store<any> = configureStore({}, history);

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App>
        <AppRoutes />
      </App>
    </ConnectedRouter>
  </Provider>,
  document.getElementById("root") as HTMLElement
);
registerServiceWorker();
