import * as React from "react";
import { Route, Switch } from "react-router";

import map from "../../common/map";
import routed from "../../common/routed";
import { routingConfig } from "../../config/routing";
import * as Domain from "../../typings/iapp";
import { IAppRoute } from "../../typings/inav";
import { IState } from "../../typings/state";

type Properties = Domain.IAppComponentProperties;
@routed()
@map(AppRoutes.mapStateToProps, undefined)
export default class AppRoutes extends React.Component<Properties, {}> {
  public static mapStateToProps(state: IState): any {
    const props: Properties = {
      authenticated: state.loading
    };
    return props;
  }

  constructor(props: Properties) {
    super(props);
  }

  public render() {
    return (
      <Switch>
        {routingConfig.map((route: IAppRoute) => (
          <Route
            key={route.path}
            path={route.path}
            exact={route.exact}
            component={route.component}
          />
        ))}
      </Switch>
    );
  }
}
