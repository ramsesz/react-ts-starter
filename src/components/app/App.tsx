import "./App.css";

import { Breadcrumb, Layout } from "antd";
import * as React from "react";
import { Link } from "react-router-dom";

import { getBreadCrumbs } from "../../common/breadcrumbs";
import map from "../../common/map";
import routed from "../../common/routed";
import { routingConfig } from "../../config/routing";
import { IRoutableProperties } from "../../types/properties";
import * as Domain from "../../typings/iapp";
import { IState } from "../../typings/state";
import Preloader from "../common/Preloader";
import Navigation from "../layout/Navigation";

// tslint:disable-next-line:no-implicit-dependencies
// tslint:disable-next-line:no-submodule-imports
const { Header, Content, Footer } = Layout;

type Properties = Domain.IAppComponentProperties & IRoutableProperties;
@routed()
@map(App.mapStateToProps, undefined)
export default class App extends React.Component<Properties, {}> {
  public static mapStateToProps(state: IState, ownProps: Properties): any {
    const props: Properties = {
      ...ownProps,
      loading: state.loading
    };
    return props;
  }

  constructor(props: Properties) {
    super(props);
  }

  public render() {
    const { location, loading } = this.props;

    return (
      <Layout className="layout">
        <Header>
          {/* <div className="logo" /> */}
          <Navigation />
        </Header>
        <Content style={{ padding: "0 50px" }}>
          {location && (
            <Breadcrumb style={{ margin: "16px 0" }}>
              {this.renderBreadCrumbs()}
            </Breadcrumb>
          )}
          <div style={{ background: "#fff", padding: 24, minHeight: 280 }}>
            {loading ? <Preloader /> : this.props.children}
          </div>
        </Content>
        <Footer style={{ textAlign: "center" }}>
          Conamo ©2018 Created by Rombit
        </Footer>
      </Layout>
    );
  }

  private renderBreadCrumbs: () => JSX.Element[] = () => {
    const pathSnippets = location.pathname.split("/").filter(i => i);
    const extraBreadcrumbItems = pathSnippets.map((_, index) => {
      const url = `/${pathSnippets.slice(0, index + 1).join("/")}`;
      return (
        <Breadcrumb.Item key={url}>
          <Link to={url}>{getBreadCrumbs(routingConfig)[url]}</Link>
        </Breadcrumb.Item>
      );
    });

    return [
      <Breadcrumb.Item key="home">
        <Link to="/">Home</Link>
      </Breadcrumb.Item>
    ].concat(extraBreadcrumbItems);
    // tslint:disable-next-line:semicolon
  };
}
