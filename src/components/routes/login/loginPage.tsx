import * as React from "react";
import Login from "../../auth/Login";

class LoginPage extends React.Component<{}, {}> {
  public render() {
    return (
      <div className="loginPage">
        <Login />
      </div>
    );
  }
}

export default LoginPage;
