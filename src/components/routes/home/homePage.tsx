import { Button } from "antd";
import * as React from "react";

export default class HomePage extends React.Component<{}, {}> {
  public render() {
    return (
      <div className="home">
        <Button type="primary">Primary</Button>
      </div>
    );
  }
}
