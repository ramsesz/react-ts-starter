import * as React from "react";
import { firebaseConnect } from "react-redux-firebase";

import map from "../../../common/map";
import { IState } from "../../../typings/state";

interface ILocalState {
  items: any[];
}

interface IProperties {
  userId?: any;
  activities?: firebase.database.DataSnapshot;
  firebase: any;
}
@firebaseConnect(["activities"])
@map(ActivitiesPage.mapStateToProps)
class ActivitiesPage extends React.Component<IProperties, ILocalState> {
  public static mapStateToProps(
    state: IState,
    ownProps: IProperties
  ): IProperties {
    return {
      ...ownProps,
      activities: state.firebase.data.activities
    };
  }

  constructor(props: IProperties) {
    super(props);

    this.state = {
      items: []
    };
  }

  public render() {
    const { activities } = this.props;

    console.log(activities);

    return (
      <div className="activitiesPage">
        {activities &&
          Object.keys(activities).map((item: any, i: number) => (
            <li key={i}>{item}</li>
          ))}
      </div>
    );
  }
}

export default ActivitiesPage;
