import "./Navigation.css";

import { Menu } from "antd";
import * as React from "react";
import { Link } from "react-router-dom";

import routed from "../../common/routed";
import { ROUTE_HOME } from "../../config/routes";
import { routingConfig } from "../../config/routing";
import { IRoutableProperties } from "../../types/properties";
import { IAppRoute } from "../../typings/inav";

type Properties = {} & IRoutableProperties;
@routed()
class Navigation extends React.Component<Properties, {}> {
  public constructor(props: Properties) {
    super(props);
  }

  public render() {
    return (
      <Menu
        mode="horizontal"
        selectedKeys={[
          this.props.location ? this.props.location.pathname : ROUTE_HOME
        ]}
        defaultSelectedKeys={[ROUTE_HOME]}
        style={{ lineHeight: "64px" }}
      >
        {this.renderLinks()}
      </Menu>
    );
  }

  private renderLinks: () => JSX.Element[] = () => {
    return routingConfig
      .filter(route => route.includeNavigation)
      .map((route: IAppRoute) => this.renderLink(route));
    // tslint:disable-next-line:semicolon
  };

  private renderLink: (router: IAppRoute) => JSX.Element = (
    route: IAppRoute
  ) => {
    return (
      <Menu.Item key={route.path}>
        <Link to={route.path}>{route.title}</Link>
      </Menu.Item>
    );
    // tslint:disable-next-line:semicolon
  };
}

export default Navigation;
