export const ROUTE_ACTIVITIES: string = "/activities";
export const ROUTE_ACTIVITIES_DETAIL: string = "/activities/:id";
export const ROUTE_HOME: string = "/";
export const ROUTE_LOGIN: string = "/login";
