export const FIREBASE_API_KEY = "AIzaSyD8gCAlW8LNPc2RM_556pJHciTIOj3nYq0";

// Only needed if using Firebase Realtime Database
export const FIREBASE_DATABASE_URL = "https://conamo-1832d.firebaseio.com/";

// Only needed if using Firebase Authentication
// export const FIREBASE_AUTH_DOMAIN = "https://conamo-1832d.firebaseio.com/";

// Only needed if using Firebase Storage
// export const FIREBASE_STORAGE_BUCKET = "https://conamo-1832d.firebaseio.com/";

export const fireBaseConfig = {
  apiKey: "AIzaSyD8gCAlW8LNPc2RM_556pJHciTIOj3nYq0",
  authDomain: "conamo-1832d.firebaseapp.com",
  databaseURL: "https://conamo-1832d.firebaseio.com",
  messagingSenderId: "561766595481",
  projectId: "conamo-1832d",
  storageBucket: "conamo-1832d.appspot.com"
};
