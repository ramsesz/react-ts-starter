import { RouteComponentProps } from "react-router";

import ActivitiesDetailPage from "../components/routes/activities/activitiesDetailPage";
import ActivitiesPage from "../components/routes/activities/activitiesPage";
import HomePage from "../components/routes/home/homePage";
import LoginPage from "../components/routes/login/loginPage";
import { IAppRoute } from "../typings/inav";
import * as ROUTES from "./routes";

export const createRoute: (
  path: string,
  title: string,
  component:
    | React.ComponentType<RouteComponentProps<any>>
    | React.ComponentType<any>,
  exact?: boolean,
  includeNavigation?: boolean
) => IAppRoute = (
  path,
  title,
  component,
  exact = true,
  includeNavigation = true
) => {
  return {
    component,
    exact,
    includeNavigation,
    path,
    title
  };
};

export const routingConfig: IAppRoute[] = [
  createRoute(ROUTES.ROUTE_HOME, "Home", HomePage, true),
  createRoute(ROUTES.ROUTE_ACTIVITIES, "Activities", ActivitiesPage),
  createRoute(
    ROUTES.ROUTE_ACTIVITIES_DETAIL,
    "Activity",
    ActivitiesDetailPage,
    false,
    false
  ),
  createRoute(ROUTES.ROUTE_LOGIN, "Login", LoginPage, false, false)
];
