export interface IState {
  authenticated?: boolean;
  firebase?: any;
  loading?: boolean;
}
