export interface IAppComponentProperties {
  authenticated?: boolean;
  loading?: boolean;
}
