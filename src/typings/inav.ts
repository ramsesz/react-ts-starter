import { RouteComponentProps } from "react-router";

export interface IAppRoute {
  component:
    | React.ComponentType<RouteComponentProps<any>>
    | React.ComponentType<any>;
  includeNavigation: boolean;
  path: string;
  title: string;
  exact?: boolean;
}

export interface IBreadCrumb {
  [s: string]: string;
}
