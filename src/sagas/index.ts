// tslint:disable-next-line:no-submodule-imports
import { all } from "redux-saga/effects";

// import { feedSaga } from "./feedSaga";
// import { firebaseSaga } from "./firebaseSaga";
// import { geoSaga } from "./geoSaga";
// import busySaga from "./busy";

export default function* rootSaga() {
  yield all([
    // ... all your sagas here
    // busySaga(),
    // geoSaga(),
    // feedSaga(),
    // firebaseSaga(),
  ]);
}
