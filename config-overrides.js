const tsImportPluginFactory = require("ts-import-plugin");
const { getLoader } = require("react-app-rewired");
const rewireLess = require("react-app-rewire-less");

module.exports = function override(config, env) {
  const tsLoader = getLoader(
    config.module.rules,
    rule =>
      rule.loader &&
      typeof rule.loader === "string" &&
      rule.loader.includes("ts-loader")
  );

  tsLoader.options = {
    getCustomTransformers: () => ({
      before: [
        tsImportPluginFactory({
          libraryDirectory: "es",
          libraryName: "antd",
          style: true
        })
      ]
    })
  };

  config = rewireLess.withLoaderOptions({
    modifyVars: {
      "@btn-primary-bg": "#ed3348",
      "@font-family": "'Cairo', sans-serif",
      "@layout-body-background": "#f0f2f5",
      "@layout-header-background": "#f0f2f5",
      "@primary-color": "#ed3348"
    }
  })(config, env);

  return config;
};
